from mininet.log import setLogLevel, info
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import Controller,RemoteController
from mininet.cli import CLI
from mininet.link import TCLink
#from mininet.link import Link, Intf

def aggNet():
        CONTROLLER_IP="127.0.0.1"
        net = Mininet(topo=None,build=False,link=TCLink)
        #add controller
        c0 = net.addController('c0',controller=RemoteController,ip=CONTROLLER_IP)
        #add switch
        switch =net.addSwitch('s1')
        #add host and links
        for h in range(2):
                host=net.addHost('h%s' % (h+1),ip='0.0.0.0')
                net.addLink(host,switch,delay='1ms')
	switch2 =net.addSwitch('s2')
	net.addLink(switch,switch2)
	host=net.addHost('h3',ip='0.0.0.0')
	net.addLink(host,switch2,delay='2s')
	switch3=net.addSwitch('s3')
	net.addLink(switch2,switch3,delay='1ms')
	for h in range(3,5):
                host=net.addHost('h%s' % (h+1),ip='0.0.0.0')
                net.addLink(host,switch3,delay='1ms')

        net.start()
        CLI( net )
        net.stop()

if __name__ == '__main__':
        setLogLevel( 'info' )
        aggNet()
